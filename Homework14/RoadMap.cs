﻿using Homework14.Base;
using Homework14.Base.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Homework14
{
    public class RoadMap<T, K> : Graph<T, K> where T : INode<K> where K: City
    {
        public string GetFastestWay(K startCity, K endCity)
        {
            var (startNode, endNode) = ValidateArgs(startCity, endCity);

            InintData();

            AssignWeightToNodes(startNode);
            var route = BuildRoute(startNode, endNode);
            var result = GetStringRoute(route);

            InintData();

            return result;
        }

        private (INode<K> startNode, INode<K> endNode) ValidateArgs(K startCity, K endCity)
        {
            if (startCity == null)
                throw new ArgumentNullException(nameof(startCity));
            if (endCity == null)
                throw new ArgumentNullException(nameof(endCity));

            var startNode = GetNode(startCity);
            var endNode = GetNode(endCity);
            if (!Contains(startNode))
                throw new ArgumentException($"City {startCity} does not exist in graph");
            if (!Contains(endNode))
                throw new ArgumentException($"City {startCity} does not exist in graph");

            return (startNode, endNode);
        }

        private void AssignWeightToNodes(INode<K> startNode)
        {
            var nextNode = startNode;
            var currentNode = startNode;
            int currentMinWeight = int.MaxValue;
            var minWeight = int.MaxValue;
            startNode.MinWeight = 0;

            foreach (var node in Nodes)
            {
                node.Visited = true;
                var applicableLinks = Links.Where(link => link.StartNode.Equals(currentNode));
                foreach (var link in applicableLinks)
                {
                    if (!link.EndNode.Visited)
                    {
                        minWeight = link.StartNode.MinWeight + link.Weight;
                        if (minWeight < link.EndNode.MinWeight)
                            link.EndNode.MinWeight = minWeight;
                        if (link.Weight < currentMinWeight)
                        {
                            currentMinWeight = link.Weight;
                            nextNode = link.EndNode;
                        }
                    }
                }
                currentNode = nextNode;
            }
        }

        private Dictionary<int, ILink<T, K>> BuildRoute(INode<K> startNode, INode<K> endNode)
        {
            var minWeight = 0;
            var routeOrder = -1;
            var route = new Dictionary<int, ILink<T, K>>();
            var currentNode = endNode;
            while (!currentNode.Equals(startNode))
            {
                var applicableLinks = Links.Where(link => link.StartNode.Equals(currentNode));
                foreach (var link in applicableLinks)
                {
                    minWeight = link.StartNode.MinWeight - link.Weight;
                    if (minWeight == link.EndNode.MinWeight)
                    {
                        currentNode = link.EndNode;
                        route.Add(++routeOrder, link);
                        break;
                    }
                }
            }
            return route;
        }

        private string GetStringRoute(Dictionary<int, ILink<T, K>> route)
        {
            var result = new StringBuilder();
            int distance = 0;
            foreach (var item in route.OrderByDescending(node => node.Key))
            {

                var applicableLink = Links.First(link => link.StartNode.Equals(item.Value.StartNode)
                                                        && link.EndNode.Equals(item.Value.EndNode));
                distance += applicableLink.Weight;
                result.AppendLine($"{applicableLink}");
            }
            result.AppendLine($"Total distance - {distance} km");
            return result.ToString();
        }
    }
}
