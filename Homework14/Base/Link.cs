﻿using Homework14.Base.Interfaces;

namespace Homework14.Base
{
    public class Link<T, K> : ILink<T, K> where T : INode<K>
    {
        public T StartNode { get; set; }
        public T EndNode { get; set; }
        public int Weight { get; set; }
        public override string ToString() => $"Range from {StartNode.Data} to {EndNode.Data} - {Weight} km";
    }
}
