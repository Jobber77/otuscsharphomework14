﻿using Homework14.Base.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Homework14.Base
{
    public class Graph<T, K> where T : INode<K>
    {
        public List<T> Nodes { get; set; }
        public List<Link<T, K>> Links { get; set; }

        public Graph()
        {
            Nodes = new List<T>();
            Links = new List<Link<T, K>>();
        }

        public void Add(T node) => Nodes.Add(node);

        public bool Contains(T node) => Nodes.Any(n => n.Equals(node));

        public void Remove(T node)
        {
            var toDelete = Nodes.FirstOrDefault(n => n.Equals(node));
            if (toDelete != null)
                Nodes.Remove(toDelete);
        }

        public K GetNodeData(int nodeId)
        {
            var result = Nodes.FirstOrDefault(node => node.Id == nodeId);
            return result == null ? default : result.Data;
        }

        public T GetNode(K data) => Nodes.FirstOrDefault(node => node.Data.Equals(data));

        public IEnumerable<ILink<T, K>> GetLinksForNode(INode<K> node) =>
            Links.Where(link => link.StartNode.Equals(node) || link.EndNode.Equals(node)).ToList();

        public IEnumerable<ILink<T, K>> GetLinksWithWeight(int weight) => Links.Where(link => link.Weight == weight).ToList();

        public void SetMinWeight(int id, int minWeight)
        {
            var node = Nodes.FirstOrDefault(n => n.Id == id);
            if (node != null)
                node.MinWeight = minWeight;
        }

        public void InintData()
        {
            foreach (var node in Nodes)
            {
                node.Reset();
            }
            foreach (var link in Links)
            {
                link.StartNode = Nodes.Single(node => node.Equals(link.StartNode));
                link.EndNode = Nodes.Single(node => node.Equals(link.EndNode));
            }
        }
    }
}
