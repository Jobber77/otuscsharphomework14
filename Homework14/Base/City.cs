﻿namespace Homework14.Base
{
    public class City
    {
        public string Name { get; set; }
        public override string ToString() => Name;
        public override bool Equals(object obj) => obj is City city ? city.Name == this.Name : false;
        public override int GetHashCode() => Name.GetHashCode();
    }
}
