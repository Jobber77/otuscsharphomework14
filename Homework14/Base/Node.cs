﻿using Homework14.Base.Interfaces;

namespace Homework14.Base
{
    public class Node<T> : INode<T>
    {
        private const int DEFAULT_WEIGHT = int.MaxValue;
        public int Id { get; set; }
        public T Data { get; set; } = default;
        public int MinWeight { get; set; } = DEFAULT_WEIGHT;
        public bool Visited { get; set; }
        public void Reset() 
        {
            MinWeight = DEFAULT_WEIGHT;
            Visited = false;
        }
        public override bool Equals(object obj) => obj is Node<T> node ? node.Id == this.Id : false;
        public override int GetHashCode() => Id.GetHashCode();
        public override string ToString() => $"Node {{ Id: {{ {Id} }}, Data: {{ {Data} }}, MinWeight: {{ {MinWeight} }}";
    }
}
