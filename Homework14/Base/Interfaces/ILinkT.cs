﻿namespace Homework14.Base.Interfaces
{
    public interface ILink<T, K> where T : INode<K>
    {
        T StartNode { get; set; }
        T EndNode { get; set; }
        int Weight { get; set; }
    }
}
