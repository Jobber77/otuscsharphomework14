﻿namespace Homework14.Base.Interfaces
{
    public interface INode<T>
    {
        int Id { get; set; }
        T Data { get; set; }
        int MinWeight { get; set; }
        bool Visited { get; set; }
        void Reset();
    }
}
