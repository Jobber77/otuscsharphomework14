﻿using Homework14.Base;
using Homework14.Base.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace Homework14
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var stream = new StreamReader("./Resources/graphData.json"))
            {
                Console.WriteLine("Available cities:");
                RoadMap<Node<City>, City> graph = JsonSerializer.Deserialize<RoadMap<Node<City>, City>>(stream.ReadToEnd());
                foreach (var city in graph.Nodes)
                {
                    Console.WriteLine(city.Data);
                }

                try
                {
                    Console.WriteLine("Enter start city: ");
                    var startCity = Console.ReadLine();
                    Console.Write("Enter end city: ");
                    string endCity = Console.ReadLine();
                    var fastestWay = graph.GetFastestWay(new City { Name = startCity }, new City { Name = endCity });
                    Console.WriteLine(fastestWay);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                Console.ReadKey();
            }
        }
    }
}
